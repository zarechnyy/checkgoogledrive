//
//  ViewController.swift
//  CheckDriveProj
//
//  Created by Yaroslav Zarechnyy on 1/19/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST


class ViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {
    
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
            nameUser.text = "\(user.profile.name as String)"
            
            guard let url = user.profile.imageURL(withDimension: 50) else {return}
            let data = try? Data(contentsOf: url)
            if let imageData = data {
                let image = UIImage(data: imageData)
                imgProfile.image = image
            }
        } else {
            print("\(error.localizedDescription)")
        }
    }
 
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpGoogleSignIn()
        
//        UserModel.listFilesInFolder()
       
    }
    
    private func setUpGoogleSignIn() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = [kGTLRAuthScopeDrive]
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func logOutBtn(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
        nameUser.text = "Foo"
    }
    
}

extension ViewController: UITableViewDelegate {
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
        return cell
    }
    
    
}
